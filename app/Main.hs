{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Data.Char
import           Data.List
import           Data.Time
import           System.Directory
import           System.FilePath

type Timestamp = String
type Title = String
type Filename = String

data Config = Config {
  postDir :: FilePath
  , postDateFormat :: String
  } deriving (Read, Show)

defaultConfig :: Config
defaultConfig = Config {
  postDir = "posts"
  , postDateFormat = "%Y-%m-%d-%H%M"
  }

timestamp :: Config -> IO Timestamp
timestamp cfg =
  fmap (formatTime defaultTimeLocale (postDateFormat cfg)) getCurrentTime 

slug :: String -> String
slug = map slugChar where
  slugChar c
    | isAlphaNum c = c
    | otherwise = '-'

makeFilename :: Config -> Title -> Timestamp -> Filename
makeFilename cfg title tstamp =
  makePath $ intercalate "-" elems
  where
    elems = [tstamp, slug title]
    makePath p = postDir cfg </> p <.> "md"


writePost :: Title -> FilePath -> IO ()
writePost title f = do
  putStrLn $ "Creating post with title: " ++ f
  writeFile f contents where
    contents = unlines [
      "---"
      , "title: " ++ title
      , "---"
      ]

setup :: Config -> IO ()
setup cfg = do
  let path = postDir cfg
  r <- doesPathExist path
  if r then
    putStrLn ("Directory " ++ path ++ " exists.")
  else
    createDirectory path

loadConfig :: IO Config
loadConfig = do
  let path = ".blogtools"
  r <- doesPathExist path
  if r then
    read <$> readFile path
  else do
    putStrLn $ "Configuration file " ++ path ++ " not found, using default configuration."
    pure defaultConfig

main :: IO ()
main = do
  cfg <- loadConfig
  setup cfg
  putStrLn "Title: "
  title <- getLine
  t <- timestamp cfg
  let f = makeFilename cfg title t

  r <- doesPathExist f
  if r then
    putStrLn $ "Path: " ++ f ++ " exists."
  else writePost title f
